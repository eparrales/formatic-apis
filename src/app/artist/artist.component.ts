import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ArtistService } from '../services/provider';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.scss']
})
export class ArtistComponent implements OnInit {

  @ViewChild("q", {read: ElementRef}) q: ElementRef;
  constructor(private artistService: ArtistService) { }

  ngOnInit() {
  }

  searchIt(){
    console.log(this.q.nativeElement.value);
    this.artistService.get(this.q.nativeElement.value).subscribe();
  }

}
