import { Injectable } from '@angular/core';
import { MM_API,headers } from './provider';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  api = MM_API;
  service = "artist.search";

  artists: Object;
  searchs = [];

  constructor(private http: HttpClient, private storage: StorageService) { }

  get(q){
    
    let searched = this.searchs.find((el)=>{
      return el.q == q;
    });

    if(typeof searched == "object"){
      if(Math.abs(searched.time - new Date().getTime()) < 3000){
        console.log("No ejecute la búsqueda!!");
      }else{
        console.log("Ejecutando búsqueda");
      }
    }
    //PENDIENTE, RETORNAR ALGO A LO QUE EL COMPONENTE SE PUEDA SUSCRIBIR
    //PARA EVITAR UN ERROR CUANDO NO SE HACE LA BÚSQUEDA

    let uri = `${this.api}${this.service}`;
    let params = `?page=1&page_size=5&q_artist=${q}&s_artist_rating=desc`;
    return this.http.get(uri+params, { headers: headers }).pipe(map((data) => {
      
      this.searchs.push({
        q: q,
        time: new Date().getTime()
      });

      this.artists = data;
      this.storage.save(this.artists,"artists");
    }));
  }
}
